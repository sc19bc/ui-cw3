#ifndef PROGRESSSLIDER_H
#define PROGRESSSLIDER_H
#include <QApplication>
#include <QSlider>

using namespace std;


//Custom Implementation of QSlider
//acts as a converter, as QMediaPlayer uses qint64s, and Qslider uses ints.
class ProgressSlider : public QSlider {

Q_OBJECT

public:
    ProgressSlider() : QSlider(NULL){
        this->setOrientation(Qt::Horizontal);
        connect(this, SIGNAL(sliderMoved(int)),this, SLOT(ifValueChanged(int)));
        connect(this,SIGNAL(updateSlider(int)), this, SLOT(setValue(int)));
    }

signals:
    void sValueChanged(qint64);
    void updateSlider(int);

public slots:
    void ifValueChanged(int m) {
        emit sValueChanged(qint64(m));
    }

    void videoValueChanged(qint64 m) {
        emit updateSlider(int(m));
    }

    //sets correct range for slider when video is changed
    void changeRange(qint64 duration) {
        setRange(0, duration);
    }
};


#endif // PROGRESSSLIDER_H
