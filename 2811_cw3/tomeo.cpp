/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QScrollArea>
#include "the_player.h"
#include "the_button.h"
#include "controls.h"


using namespace std;

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}

int main(int argc, char *argv[]) {

    qDebug() << "Qt version: " << QT_VERSION_STR << endl; // check that Qt is operational first
    QApplication app(argc, argv); // create the Qt Application

    vector<TheButtonInfo> videos; // collect all the videos in the folder
    if (argc == 2)
        videos = getInfoIn( string(argv[1]) );
    if (videos.size() == 0) {
        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );
        switch( result ){
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }exit(-1);
    }

    QVideoWidget *videoWidget = new QVideoWidget; //widget for video
    videoWidget->setMinimumHeight(300);
    videoWidget->setMinimumWidth(530);

    ThePlayer *player = new ThePlayer(); //QMediaPlayer which controls the playback
    player->setVideoOutput(videoWidget);

    //create the progress slider
    ProgressSlider *progressSlider = new ProgressSlider();//makes progress slider & connections
    QObject::connect(progressSlider, SIGNAL(sValueChanged(qint64)), player, SLOT(setPosition(qint64)));
    QObject::connect(player, SIGNAL(positionChanged(qint64)), progressSlider, SLOT(videoValueChanged(qint64)));
    QObject::connect(player, SIGNAL(durationChanged(qint64)), progressSlider, SLOT(changeRange(qint64)));

    //create the widget for the video buttons
    QWidget *buttonWidget = new QWidget(); //widget for  album
    vector<TheButton*> buttons; // a list of the buttons
    QVBoxLayout *layout = new QVBoxLayout(); // the buttons are arranged vertically
    buttonWidget->setMinimumHeight(112*videos.size());
    buttonWidget->setMinimumWidth(600);
    buttonWidget->setLayout(layout);

    //create the scroll bar
    QScrollArea *scroll = new QScrollArea(); //Qscrollarea
    scroll->setWidget(buttonWidget); //set scroll on album
    scroll->setWidgetResizable(true);
    scroll->setMinimumWidth(235);
    scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QPalette pal = buttonWidget->palette();
    // set background
    pal.setColor(QPalette::Background, Qt::darkCyan);
    buttonWidget->setAutoFillBackground(true);
    buttonWidget->setPalette(pal);

    for ( int i = 0; i < videos.size()-1; i++ ) { // create the buttons
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        button->setMaximumHeight(112);
        button->setMaximumWidth(196);
        button->setMinimumHeight(112);
        buttons.push_back(button);
        layout->addWidget(button);
        button->init(&videos.at(i));
    }

    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos);

    //create controls & connect play/pause buttons
    Controls *controls = new Controls(videoWidget);
    QObject::connect(controls, SIGNAL(play()), player, SLOT(play()));
    QObject::connect(controls,SIGNAL(pause()),player, SLOT(pause()));
    QObject::connect (player, SIGNAL(stateChanged(QMediaPlayer::State)),controls, SLOT(changeState()));

    //connect skip ahead/back buttons
    QObject::connect(controls, SIGNAL(next()), player, SLOT(skipAhead()));
    QObject::connect(controls, SIGNAL(previous()), player, SLOT(skipBack()));

    //connect volume slider
    QObject::connect(controls, SIGNAL(newVol(int)), player, SLOT(setVolume(int)));

    // create the main window and layout
    QWidget window;
    QHBoxLayout *top = new QHBoxLayout();
    window.setLayout(top);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(500, 380);
    //window.setMaximumSize(2200,2000);

    pal.setColor(QPalette::Background, Qt::lightGray);
    window.setAutoFillBackground(true);
    window.setPalette(pal);

    //create title and sort buttons
    QWidget *titles = new QWidget();
    QHBoxLayout *horizontal = new QHBoxLayout();
    titles->setLayout(horizontal);
    QLabel *title = new QLabel;
    title->setText("Tomeo");
    QFont titleFont = title->font();
    titleFont.setPointSize(20);
    title->setFont(titleFont);

    QPushButton *sort_length = new QPushButton();
    sort_length->setText("Length");
    sort_length->setMinimumHeight(40);
    sort_length->setMaximumWidth(100);
    QPushButton *sort_date = new QPushButton();
    sort_date->setText("Date");
    sort_date->setMinimumHeight(40);
    sort_date->setMaximumWidth(100);
    QPushButton *sort_loc = new QPushButton();
    sort_loc->setText("Location");
    sort_loc->setMinimumHeight(40);
    sort_loc->setMaximumWidth(100);

    horizontal->addWidget(title);
    horizontal->addWidget(sort_length);
    horizontal->addWidget(sort_date);
    horizontal->addWidget(sort_loc);

    // add the titles, video and the buttons to the top level widget
    QVBoxLayout *base = new QVBoxLayout();
    top->addLayout(base);

    base->addWidget(titles);
    base->addWidget(videoWidget);
    base->addWidget(progressSlider);
    base->addWidget(controls);

    QHBoxLayout *videobase = new QHBoxLayout();
    top->addLayout(videobase);
    videobase->addWidget(scroll);

    window.show();
    return app.exec(); //terminate
}
