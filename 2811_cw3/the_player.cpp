//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);


}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

void ThePlayer::skipAhead() {
    //currentPos+5
    setPosition(position() + 1000);

}

void ThePlayer::skipBack() {
    setPosition(position() - 1000);
}


//void ThePlayer::converterToInt(qint64 num){
//    emit positionChanged(int(num));
//}

//void ThePlayer::converterToQInt(int num){
//    //emit changePosition(qint64(num));
//}
