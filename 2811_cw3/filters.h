//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_FILTER_H
#define CW2_THE_FILTER_H


#include <QPushButton>

class TheFilter : public QPushButton {
Q_OBJECT


public:
    TheFilter();
private:
     bool on;

public slots:
     void onClicked();
     int return_on();
};
#endif //CW2_THE_FILTER_H
