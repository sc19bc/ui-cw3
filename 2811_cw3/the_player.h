//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include "progress_slider.h"
#include <vector>
#include <QTimer>
#include <QSlider>

using namespace std;

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
    int buttonNumber;//keeps track of which button is playing

public:
    ThePlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
        setNotifyInterval(16);//makes it so that the progress bar will update every 16 miliseconds,
                              //enough to be smooth for 60Hz monitors
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)));
    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i);


private slots:

    void playStateChanged (QMediaPlayer::State ms);

public slots:

    void jumpTo (TheButtonInfo* button); // start playing this ButtonInfo

    void skipAhead();//
    void skipBack(); //
};

#endif //CW2_THE_PLAYER_H
