#include "controls.h"
#include <QStyle>
#include <QLayout>
#include "the_player.h"
#include <QSlider>

Controls::Controls(QWidget *parent) : QWidget(parent){
    playButton = new QToolButton(this);//makes button, sets icon
    playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    connect(playButton, SIGNAL(clicked()), this,SLOT(playClicked()));

    forwardButton = new QToolButton(this);
    forwardButton->setIcon(style()->standardIcon(QStyle::SP_MediaSeekForward));
    connect(forwardButton, SIGNAL(clicked()), this, SIGNAL(next()));

    backButton = new QToolButton(this);
    backButton->setIcon(style()->standardIcon(QStyle::SP_MediaSeekBackward));
    connect(backButton, SIGNAL(clicked()), this, SIGNAL(previous()));

    volumeButton = new QToolButton(this);
    volumeButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
    connect(volumeButton, SIGNAL(clicked()), this, SLOT(muteClicked()));

    volumeSlider = new QSlider(Qt::Horizontal, this);
    volumeSlider->setValue(0);
    volumeSlider->setSingleStep(10);
    volumeSlider->setPageStep(20);

    connect(volumeSlider, SIGNAL(valueChanged(int)), this, SIGNAL(newVol(int)));
    connect(volumeSlider, SIGNAL(valueChanged(int)), this, SLOT(switchMuteStyle(int)));

    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(volumeButton);
    layout->addWidget(volumeSlider);
    layout->addStretch(1);
    layout->addWidget(backButton);
    layout->addWidget(playButton);
    layout->addWidget(forwardButton);
    layout->addStretch(1);
    layout->addSpacing(120);
    setLayout(layout);
}

void Controls::playClicked(){

    switch (playerState) {
    case QMediaPlayer::PausedState:
        emit play();
        playerState = QMediaPlayer::PlayingState;
        break;
    case QMediaPlayer::PlayingState:
        emit pause();
        playerState = QMediaPlayer::PausedState;
        break;
    default:
        break;
    }
    switchStyle();
}

void Controls::muteClicked() {
    if (volumeSlider->sliderPosition() == 0){
        volumeSlider->setSliderPosition(1);
    }else {
        volumeSlider->setSliderPosition(0);
    }
}

void Controls::switchMuteStyle(int v) {
    if (v == 0) {
        volumeButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
    }else {
        volumeButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
    }

}

void Controls::switchStyle(){

    if (playerState == QMediaPlayer::PausedState){
        playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    }else {
        playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    }
}

//called whenever the player's StateChanged is called.
void Controls::changeState() {
    playerState = QMediaPlayer::PlayingState;
    switchStyle();
}
