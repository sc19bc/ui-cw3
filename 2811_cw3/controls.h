#ifndef CONTROLS_H
#define CONTROLS_H

#include <QMediaPlayer>
#include <QWidget>
#include <QToolButton>

class QSlider;

class Controls : public QWidget {
    
    Q_OBJECT
public:
    explicit Controls(QWidget *parent = nullptr);

    QMediaPlayer::State state() const;

signals:
    void play();
    void pause();
    void next();
    void previous();
    void newVol(int v);


private slots:
    void playClicked();
    void muteClicked();

public slots:
    void changeState();
    void switchMuteStyle(int);

private:
    void switchStyle();//switches the style of the button between play & pause
    QMediaPlayer::State playerState = QMediaPlayer::PlayingState;//playing is the starting state
    QToolButton *playButton;
    QToolButton *forwardButton;
    QToolButton *backButton;
    QToolButton *volumeButton;
    QSlider *volumeSlider;
};

#endif // CONTROLS_H
